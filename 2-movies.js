const favouritesMovies = {
  Matrix: {
    imdbRating: 8.3,
    actors: ["Keanu Reeves", "Carrie-Anniee"],
    oscarNominations: 2,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$680M",
  },
  FightClub: {
    imdbRating: 8.8,
    actors: ["Edward Norton", "Brad Pitt"],
    oscarNominations: 6,
    genre: ["thriller", "drama"],
    totalEarnings: "$350M",
  },
  Inception: {
    imdbRating: 8.3,
    actors: ["Tom Hardy", "Leonardo Dicaprio"],
    oscarNominations: 12,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$870M",
  },
  "The Dark Knight": {
    imdbRating: 8.9,
    actors: ["Christian Bale", "Heath Ledger"],
    oscarNominations: 12,
    genre: ["thriller"],
    totalEarnings: "$744M",
  },
  "Pulp Fiction": {
    imdbRating: 8.3,
    actors: ["Sameul L. Jackson", "Bruce Willis"],
    oscarNominations: 7,
    genre: ["drama", "crime"],
    totalEarnings: "$455M",
  },
  Titanic: {
    imdbRating: 8.3,
    actors: ["Leonardo Dicaprio", "Kate Winslet"],
    oscarNominations: 13,
    genre: ["drama"],
    totalEarnings: "$800M",
  },
};

// Q1. Find all the movies with total earnings more than $500M.

//Enter the amount in number only
function greaterMovieEarnings(amount, object) {
  const resultArray = Object.entries(object);

  return resultArray.reduce((accumulator, current) => {
    if (
      parseInt(
        current[1].totalEarnings.slice(1, current[1].totalEarnings.length - 1)
      ) > amount
    ) {
      accumulator[current[0]] = current[1];
    }

    return accumulator;
  }, {});
}

// console.log(greaterMovieEarnings(500, favouritesMovies));

// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

function greaterOscarNominations(number, amount, object) {
  return Object.entries(greaterMovieEarnings(amount, object)).reduce(
    (accumulator, current) => {
      if (current[1].oscarNominations > number) {
        accumulator[current[0]] = current[1];
      }
      return accumulator;
    },
    {}
  );
}

// console.log(greaterOscarNominations(3, 500, favouritesMovies));

//Q.3 Find all movies of the actor "Leonardo Dicaprio".

function moviesWithActor(actor, object) {
  return Object.entries(object).reduce((accumulator, current) => {
    if (current[1].actors.includes(actor)) {
      accumulator[current[0]] = current[1];
    }
    return accumulator;
  }, {});
}

// console.log(moviesWithActor("Leonardo Dicaprio", favouritesMovies));

/* Q.4 Sort movies (based on IMDB rating)
if IMDB ratings are same, compare totalEarning as the secondary metric.
*/
function sortMovies(object) {
  return Object.entries(object)
    .sort((currentMovie, nextMovie) => {
      if (currentMovie[1].imdbRating === nextMovie[1].imdbRating) {
        return (
          parseInt(currentMovie[1].totalEarnings.slice(1)) -
          parseInt(nextMovie[1].totalEarnings.slice(1))
        );
      } else {
        return currentMovie[1].imdbRating - nextMovie[1].imdbRating;
      }
    })
    .reduce((accumulator, current) => {
      accumulator[current[0]] = current[1];
      return accumulator;
    }, {});
}
// console.log(sortMovies(favouritesMovies));

/*Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
    drama > sci-fi > adventure > thriller > crime
*/

function groupMovies(object) {
  return Object.entries(object).reduce((accumulator, current) => {
    current[1].genre.sort((currentGenre, nextGenre) => {
      if (
        currentGenre === "drama" &&
        (nextGenre === "sci-fi" ||
          nextGenre === "adventure" ||
          nextGenre === "thriller" ||
          nextGenre === "crime")
      ) {
        return -1;
      } else if (
        currentGenre === "sci-fi" &&
        (nextGenre === "adventure" ||
          nextGenre === "thriller" ||
          nextGenre === "crime")
      ) {
        return -1;
      } else if (
        currentGenre === "adventure" &&
        (nextGenre === "thriller" || nextGenre === "crime")
      ) {
        return -1;
      } else if (currentGenre === "thriller" && nextGenre === "crime") {
        return -1;
      } else {
        return 1;
      }
    });

    if (accumulator.hasOwnProperty(current[1].genre[0])) {
      accumulator[current[1].genre[0]].push(current[0]);
    } else {
      accumulator[current[1].genre[0]] = [current[0]];
    }
    return accumulator;
  }, {});
}

// console.log(groupMovies(favouritesMovies));
